// ==UserScript==
// @name         Pessoas no andar
// @namespace    https://*.neowrk.com/desk4me/desk/booking
// @version      0.7
// @description  Ver quem agendou vaga no andar
// @author       Humberto S. N. dos Anjos
// @match        https://*.neowrk.com/desk4me/desk/booking
// @icon         https://www.google.com/s2/favicons?domain=neowrk.com/desk4me
// @require      https://code.jquery.com/jquery-3.6.0.slim.js
// @grant        none
// ==/UserScript==

/* ordenador de mesas */
function deskOrderAsc(a, b) {
    return +(a.desk > b.desk) || +(a.desk === b.desk) - 1;
}

/* extrai os lugares agendados */
function getBookingsFrom(data) {
    return data.filter(function (v) { return v.bookings.length > 0; })
        .map(function (v) { return { desk: v.desk.name, users: v.bookings.map(function (b) { return b.userName; }).join(', ') }; })
        .sort(deskOrderAsc);
}

/* elementos HTML */
function contentAsHTML(bookings) {
    let lis = bookings.map(function (v) { return '<li style="text-align: left">' + v.desk + ': ' + v.users + '</li>'; }).join('');
    return '<ul>' + lis + '</ul>';
}

function headerAsHTML(bookings) {
    return '<span>' + bookings.length + ' lugar(es) agendado(s)</span>';
}

/* atualiza o conteúdo publicado */
function fillBookingsList(bookings) {
    $('#bookingsList').html(
        headerAsHTML(bookings) +
        contentAsHTML(bookings)
    );
}

/* intercepta a requisição que pega os dados relevantes e atualiza o resultado publicado */
let bookings = []; // é preciso armazenar o primeiro resultado, pois o elemento HTML para publicação pode não ter sido criado ainda. Depois esta variável é desnecessária

function isBookingsRequest(response) {
    return response.readyState === 4 && !!(response.responseURL.match(/\/desks-with-bookings/g));
}

(function(open) {
    XMLHttpRequest.prototype.open = function() {
        this.addEventListener("readystatechange", 
            function() {
                if(isBookingsRequest(this)) {
                    bookings = getBookingsFrom(JSON.parse(this.response));

                    fillBookingsList(bookings);
                }
            }, 
            false);
        open.apply(this, arguments);
    };
})(XMLHttpRequest.prototype.open);

/* a página é um SPA, então é preciso aguardar o DOM ser montado antes de tentar modificá-lo */
function tryAddBookingsList() {
    'use strict';

    let parent = $('div.md-card-content>div.left-panel>div.filter-desk');
    if(parent.length === 0) { // a página não terminou de desenhar ainda, não faça nada
        return false;
    }

    parent.append('<div id="bookingsList"></div>');

    fillBookingsList(bookings);

    return true;
}

function waiter() {
    if(! tryAddBookingsList()) {
        console.log('A página não terminou de montar. Aguardando...');
        setTimeout(waiter, 2000);
    } else {
        console.log('Página montada e lista adicionada.');
    }
}

setTimeout(waiter, 2000);
